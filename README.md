Una compañía automotríz te acaba de contratar para diseñar y
desarrollar un software para sus automóviles automáticos.

Te entrega la siguiente documentación:

## Marchas de un automóvil automático
*  P (Parking)
*  R (Reversa)
*  N (Neutral)
*  D (Drive)
Es en ese orden el recorrido de la palanca de cambios. Es decir, no
hay forma de pasar de **P** a **N** sin pasar por **R**.

## Detalle de los vehículos que construímos
Por ser una compañía automotríz que opera en 5 continentes la gama de
vehículos que creamos es muy amplia. Desde un automóvil sedán de 2
puertas, hasta un camión de doble remolque de veinte toneladas pasando
por las gamas ES, MID, LUX de automóviles y camionetas SUV más aptas para
el mercado global. 

## Lo que la empresa pide que cumpla la solución
Dado que construímos diferentes tipos de vehículos para el mercado
nos gustaría que tu software sea lo suficientemente robusto y flexible
para que cumpla con el estándar YHS que a nivel mundial es
requerimiento para vender las unidades, por eso es necesario:

*  Pueda adaptarse para los camiones y cualquier tipo de vehículo que
   comercializamos aunque de momento no es necesario que se implemente
   una solución más que para los automóviles sedanes.
*  Siguiendo el estándar YHS, la computadora del vehículo tendrá que
   ser capaz de calcular y visualizar amigablemente al usuario
   (conductor) la siguiente información:
   -  Kilometraje
   -  Rendimiento de combustible
   -  La marcha actual
   -  Subir de marcha
   -  Bajar de marcha
   -  Indicar si bajo las condiciones de temperatura y humedad
      exterior al vehículo puede o no haber presencia de pavimento con
      hielo.
*  Se proporcionará un archivo de pruebas que corresponden a la
   certificación CTF-20201 cuidadosamente escrito y que en caso de
   pasar las pruebas obtener la certificación, en caso contrario
   tendrán una penalización misma que se describirá en los criterios
   de evaluación.

## Criterios de evaluación

Por ser una práctica que aplicará los conceptos vistos en la materia de ICC
tanto teóricos como prácticos se dividirá en dos partes su entrega.

*  **Parte 1. Abstracción y diseño** Los entregables serán:
   -  Diagramas de clases.
   -  Arquitectura y separación de responsabilidades.
   -  Uso de las herramientas que provée el lenguaje de programación para
      facilitar entendimiento y desarollo.
   -  Identificación de entradas y salidas.

*  **Parte 2. Implementación de la solución** Esta parte será el conjunto de
   clases que den solución al problema planteado, para poder pasar a esta fase
   es necesario el visto bueno (Vo.Bo) del ayudante del laboratorio en la fase
   1. Los entregables serán:
   -  Proyecto en Maven.
   -  Conjunto de clases.
   -  Conjunto de pruebas.

*  **Evaluación** 
   -  Todo proyecto deberá ser entregado en su totalidad para ser
      acreedor a calificación.
   -  Proyecto que no compile se calificará a cero.
   -  El código deberá tener documentación en formato JavaDoc.
   -  Las pruebas deben pasar para evaluación.
   -  Legibilidad y simpleza en el código.

*  **Consideraciones adicionales** 
   -  Tendrán 2 revisiones mismas que incluyen tips y observaciones para
      mejorar su implementación.
   -  Después de estás revisiones se considerará entregado el proyecto.
   -  Las revisiones dependiendo de la cantidad de observaciones se
      acordará con cada quién el tiempo de entrega y una vez definido este
      será improrrogable y estrictamente deberá ser cumplido. En caso de no
      serlo, se calificará sobre la primer revisión realizada.
