package unam.fciencias.icc;
import javax.swing.*;
import java.awt.*;

public class InterfazGráfica {
    private JFrame frame;
    private JButton bn, bs, bw, be, bc;

    public InterfazGráfica() {
        frame = new JFrame("Panel de control");
        bn = new JButton("Crear automovil");
        bs = new JButton("Presencia de hielo");
        bw = new JButton("Subir marcha");
        be = new JButton("Bajar Marcha");
        bc = new JButton("Estado actual");
    }

    public void launchFrame() {
        frame.add(bn, BorderLayout.NORTH);
        frame.add(bs, BorderLayout.SOUTH);
        frame.add(bw, BorderLayout.WEST);
        frame.add(be, BorderLayout.EAST);
        frame.add(bc, BorderLayout.CENTER);
        frame.setSize(400,200);
        frame.setVisible(true);
    }
}
