package unam.fciencias.icc;

public class Clima {
    private double temperatura;
    private double humedad;

    public double getTemperatura() {
        return this.temperatura;
    }

    public double getHumedad() {
        return this.humedad;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public void setHumedad(double humedad) {
        this.humedad = humedad;
    }

    public boolean presenciaHielo() {
        if(this.humedad >= 50 && this.temperatura <= 0) {
            return true;
        } else return false;
    }

    public String toString() {
        return "Humedad al " + humedad + "%. Temperatura de " + temperatura + " °C.";
    }
}