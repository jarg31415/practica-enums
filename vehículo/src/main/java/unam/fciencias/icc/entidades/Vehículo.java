package unam.fciencias.icc.entidades;

public abstract class Vehículo {
    public enum Marcha {
        P("Parking", 1),
        R("Reversa", 2),
        N("Neutral", 3),
        D("Drive", 4);

        private final String marcha;
        private final int valor;

        Marcha(String marcha, int valor) {
            this.marcha = marcha;
            this.valor = valor;
        }

        public String getMarcha() {
            return this.marcha;
        }

        public int getValor() {
            return this.valor;
        }
    }

    private double kilometraje;
    private double nivelCombustible;
    private Marcha marcha;

    public double getKilometraje() {
        return this.kilometraje;
    }

    public double getNivelDeCombustible() {
        return this.nivelCombustible;
    }

    public Marcha getMarcha() {
        return this.marcha;
    }

    public void setKilometraje(double kilometraje) {
        this.kilometraje = kilometraje;
    }

    public void setNivelDeCombustible(double nivelDeCombustible) {
        this.nivelCombustible = nivelDeCombustible;
    }

    protected void setMarcha(Marcha marcha) {
        this.marcha = marcha;
    }

   public double rendimientoDeCombustible(double distancia, double combustible, boolean esInternacional) {
       if (esInternacional) {
           return distancia / combustible;
       } else {
           return (distancia * 0.621371) / (combustible * 0.264172);
       }
   }

   abstract public void subirMarcha();

    abstract public void bajarMarcha();
}